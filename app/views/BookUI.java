package views;

import com.google.common.base.Function;
import com.google.common.collect.FluentIterable;
import models.java.Author;
import models.java.Book;

import java.util.List;

/**
 * Date: 3/5/14
 * Time: 9:19 PM
 */
public class BookUI {

    public static final Function<Author,AuthorUI> UI_FUNCTION = new Function<Author, AuthorUI>() {
        public AuthorUI apply(Author input) {
            return new AuthorUI(input);
        }
    };
    private Book book;

    public BookUI(Book book){
        this.book = book;
    }

    public String getTitle(){
        return book.getTitle();
    }

    public String getIsbn(){
        return book.getIsbn();
    }

    public List<AuthorUI> getAuthors(){
        return FluentIterable.from(book.getAuthors()).transform(UI_FUNCTION).toList();
    }

    public static class AuthorUI {
        private Author author;

        public AuthorUI(Author author) {
            this.author = author;
        }

        public String getFirstName(){
            return author.getFirstName();
        }

        public String getLastName(){
            return author.getLastName();
        }
    }
}
