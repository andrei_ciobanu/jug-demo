package models.scala

import play.api.db.slick.Config.driver.simple._

/**
 * Date: 3/5/14
 * Time: 10:20 PM
 */
case class Book(id: Option[Int] = None, title: String, isbn: String)

class Books(tag: Tag) extends Table[Book](tag, "book"){
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def title = column[String]("title")
  def isbn = column[String]("isbn")

  def * = (id.?, title, isbn) <> (Book.tupled, Book.unapply)
}
