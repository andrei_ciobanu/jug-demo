package models.scala

import play.api.db.slick.Config.driver.simple._

/**
 * Date: 3/5/14
 * Time: 10:24 PM
 */
case class BookAuthor(id: Option[Int] = None, bookId: Int, authorId: Int)

class BookAuthors(tag: Tag) extends Table[BookAuthor](tag, "book_author"){
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def bookId = column[Int]("book_id")
  def authorId = column[Int]("author_id")

  def * = (id.?, bookId, authorId) <> (BookAuthor.tupled, BookAuthor.unapply)
}
