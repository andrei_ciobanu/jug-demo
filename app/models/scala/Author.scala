package models.scala

// Importing the slick driver
import play.api.db.slick.Config.driver.simple._


/**
 * Date: 3/5/14
 * Time: 10:08 PM
 */
case class Author(id: Option[Int] = None, firstName: String, lastName: String)


class Authors(tag: Tag) extends Table[Author](tag, "author"){
  def id = column[Int]("id", O.PrimaryKey, O.AutoInc)
  def firstName = column[String]("first_name")
  def lastName = column[String]("last_name")

  def * = (id.?, firstName, lastName) <> (Author.tupled, Author.unapply)
}



