package context

import play.api.libs.concurrent.Akka
import scala.concurrent.ExecutionContext
import play.api.Play.current

object Contexts {
  implicit val expensiveDbDispatcher: ExecutionContext = Akka.system.dispatchers.lookup("play.akka.actor.expensive-db-dispatcher")
}