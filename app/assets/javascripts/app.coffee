require(["webjars!knockout.js", 'webjars!jquery.js',"/routes.js", "webjars!bootstrap.js"], (ko) ->

  # Models for the messages page
  class BookModel
    constructor: () ->
      self = @
      self.book = ko.observable()
      self.error = ko.observable()

      # get the messages
      @getBook = (url) ->
        $('.retry_btn').prop('disabled', true);
        @ajax(url)
          .done((data, status, xhr) ->
            self.book(data)
            self.error('')
            $('.retry_btn').prop('disabled', false);
          )
          .error((jqXHR, textStatus, errorThrown) ->
            self.error(jqXHR.responseText)
            $('.retry_btn').prop('disabled', false);
          )

      @getBookJava = () ->
        @getBook(routes.controllers.JavaBookController.getRandomBook())

      @getBookScala = () ->
        @getBook(routes.controllers.ScalaBookController.getRandomBook())

    # Convenience ajax request function
    ajax: (route, params) ->
      $.ajax($.extend(params, route))

  class AuthorModel
    constructor: () ->
        self = @
        self.firstName = ko.observable()
        self.lastName = ko.observable()

  # Setup
  model = new BookModel
  ko.applyBindings(model)

)
