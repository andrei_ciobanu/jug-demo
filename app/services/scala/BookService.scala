package services.scala

import play.api.db.slick.Config.driver.simple._
import models.scala._
import scala.util.Random
import play.Logger._

/**
 * Date: 3/5/14
 * Time: 10:32 PM
 */
object BookService {

  val authors = TableQuery[Authors]

  val books = TableQuery[Books]

  val bookAuthors = TableQuery[BookAuthors]

  def randomBook()(implicit rs: Session): (Option[Book], List[Author]) = {
    debug("start Scala randomBook")
    books.map(_.id).max.run.map( maxId => {
      val random: Int = Random.nextInt(maxId) + 1
      val bookVal = books.filter(_.id >= random).take(1).list().headOption;
      val authorsVal = getAuthorsForBook(bookVal)
      debug("end Scala randomBook")
      (bookVal, authorsVal)}
    ).getOrElse((None,List()))
  }


  def getAuthorsForBook(bookVal: Option[Book])(implicit rs: Session): List[Author] = {
    bookVal.map(b => (for {
      ba <- bookAuthors if ba.bookId === b.id
      a <- authors if ba.authorId === a.id
    } yield a).list()).getOrElse(List())
  }
}
