package services.java;

import models.java.Book;
import play.Logger;

import java.util.List;
import java.util.Random;

import static play.db.jpa.JPA.em;

/**
 * Date: 3/5/14
 * Time: 8:44 PM
 */
public class BookService {

    public static final Random RANDOM = new Random();

    public static Book randomBook(){
        Logger.debug("end randomBook Java");
        Integer maxId = em().createQuery("select MAX(id) from Book book", Integer.class).getSingleResult();
        if(maxId == null){
            return null;
        }
        Integer random = RANDOM.nextInt(maxId) + 1;
        final Book book = singleResult(em().createQuery("select book from Book book where id >= :random", Book.class)
                .setParameter("random", random).setMaxResults(1).getResultList());
        Logger.debug("end randomBook Java");
        return book;
    }

    private static Book singleResult(List<Book> list) {
        if(list.isEmpty()){
            return null;
        } else {

            return list.iterator().next();
        }
    }

}
