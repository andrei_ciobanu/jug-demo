package controllers

import play.api.mvc._
import services.scala.BookService._
import models.scala._

import play.api.libs.json._
import play.api.libs.functional.syntax._

import scala.concurrent.Future
import context.Contexts.expensiveDbDispatcher

import play.api.db.slick._
import play.api.db.slick.Config.driver.simple.Session
import play.api.Play.current

/**
 * Date: 3/5/14
 * Time: 10:54 PM
 */
object ScalaBookController extends Controller {

  def getRandomBook = Action {
    DB.withSession { implicit rs =>
      val (book,authors) = randomBook()
      getResult((book,authors))
    }
  }

  /*
  def getRandomBook = Action.async {
    val result = Future{
      DB.withSession {
        implicit rs: Session => randomBook()}}
    result.map( result => getResult(result))
  }
  */

  def getResult(tuple: (Option[Book], List[Author])) = {
    tuple._1.map( book => Ok(toJson(book, tuple._2))).getOrElse(NotFound("no books found"))
  }


  def toJson(book: Book, authors: List[Author]) = {
    Json.obj(
      "title" -> book.title,
      "isbn" -> book.isbn,
      "authors" -> authors
    )
  }

  implicit val authorWrites: Writes[Author] = (
    ( __ \ "firstName").write[String] and
    ( __ \ "lastName").write[String]
    )(unlift(unapplyAuthor))

  def unapplyAuthor(a: Author) = {
    Some(a.firstName,a.lastName)
  }
}
