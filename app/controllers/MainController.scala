package controllers

import play.api.Routes
import play.api.mvc._

/**
 * Date: 3/6/14
 * Time: 6:04 AM
 */
object MainController extends Controller {

  def index = Action {
    Ok(views.html.index.render())
  }


  /** The javascript router. */
  def router = Action { implicit req =>
    Ok(
      Routes.javascriptRouter("routes")(
        routes.javascript.ScalaBookController.getRandomBook,
        routes.javascript.JavaBookController.getRandomBook
      )
    ).as("text/javascript")
  }


}
