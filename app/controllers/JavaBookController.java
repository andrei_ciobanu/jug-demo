package controllers;

import com.fasterxml.jackson.databind.JsonNode;
import models.java.Book;
import play.db.jpa.JPA;
import play.db.jpa.Transactional;
import play.libs.Akka;
import play.libs.F;
import play.libs.Json;
import play.mvc.Controller;
import play.mvc.Result;
import views.BookUI;

import static play.libs.F.Promise;
import static services.java.BookService.randomBook;

/**
 * Date: 3/5/14
 * Time: 8:44 PM
 */
public class JavaBookController extends Controller {

    @Transactional
    public static Result getRandomBook(){
        Book book = randomBook();
        if(book != null){
            BookUI bookUI = new BookUI(book);
            return ok(Json.toJson(bookUI));
        } else {
            return notFound("no book found");
        }
    }

    /*public static Promise<Result> getRandomBook(){
        Promise<Book> promise = Promise.promise(new F.Function0<Book>() {
            public Book apply() throws Throwable {
                return JPA.withTransaction(new F.Function0<Book>() {
                    @Override
                    public Book apply() throws Throwable {
                        return randomBook();
                    }
                });
            }
        });

        return promise.map(new F.Function<Book, Result>() {
            @Override
            public Result apply(Book book) throws Throwable {
                if(book != null){
                    BookUI bookUI = new BookUI(book);
                    return ok(Json.toJson(bookUI));
                } else {
                    return notFound("no book found");
                }
            }
        });
    }*/

    /*public static Promise<Result> getRandomBook(){
        Promise<JsonNode> promise = Promise.promise(new F.Function0<JsonNode>() {
            public JsonNode apply() throws Throwable {
                return JPA.withTransaction(new F.Function0<JsonNode>() {
                    @Override
                    public JsonNode apply() throws Throwable {
                        Book book = randomBook();
                        if(book != null){
                            BookUI bookUI = new BookUI(book);
                            return Json.toJson(bookUI);
                        } else {
                            return null;
                        }
                    }
                });
            }
        },  Akka.system().dispatchers().lookup("play.akka.actor.expensive-db-dispatcher"));

        return promise.map(new F.Function<JsonNode, Result>() {
            public Result apply(JsonNode jsonNode) throws Throwable {
                if(jsonNode != null){
                    return ok(jsonNode);
                } else {
                    return notFound("no book found");
                }
            }
        });
    }*/

}
