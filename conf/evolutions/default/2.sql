# initial schema

# --- !Ups


INSERT INTO "author" VALUES (1,'James','Clavell');
INSERT INTO "author" VALUES (2,'Martin','Odersky');
INSERT INTO "author" VALUES (3,'Lex','Spoon');
INSERT INTO "author" VALUES (4,'Bill','Venners');


INSERT INTO "book" VALUES (1, 'Shōgun', '044008721X');
INSERT INTO "book" VALUES (2, 'King Rat', '0440145465');
INSERT INTO "book" VALUES (3, 'Programming in Scala: A Comprehensive Step-by-Step Guide, 2nd Edition', '0981531644');

INSERT INTO "book_author" VALUES (1,1,1);
INSERT INTO "book_author" VALUES (2,2,1);
INSERT INTO "book_author" VALUES (3,3,2);
INSERT INTO "book_author" VALUES (4,3,3);
INSERT INTO "book_author" VALUES (5,3,4);

# --- !Downs

DELETE FROM "book_author";

DELETE FROM "author";

DELETE FROM "book";