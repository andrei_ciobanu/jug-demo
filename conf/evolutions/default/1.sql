# initial schema

# --- !Ups

CREATE TABLE "author" (
    "id" int(16) NOT NULL AUTO_INCREMENT,
    "first_name" varchar(255) NOT NULL,
    "last_name" varchar(255) NOT NULL,
    PRIMARY KEY ("id")
);

CREATE TABLE "book" (
    "id" int(16) NOT NULL AUTO_INCREMENT,
    "title" varchar(255) NOT NULL,
    "isbn" varchar(255) NOT NULL,
    PRIMARY KEY ("id")
);

CREATE TABLE "book_author" (
    "id" int(16) NOT NULL AUTO_INCREMENT,
    "book_id" int(16) NOT NULL,
    "author_id" int(16) NOT NULL,
    PRIMARY KEY ("id"),
    FOREIGN KEY ("book_id") REFERENCES "book"("id"),
    FOREIGN KEY ("author_id") REFERENCES "author"("id")
);


# --- !Downs

DROP TABLE "author";

DROP TABLE "book";